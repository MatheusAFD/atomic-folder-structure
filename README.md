# Motivações/Benefícios

- Facilita a leitura do projeto para o desenvolvedor
- Minimiza erros ao refatorar código
- Facilita a escalabilidade do sistema
- Facilita a execução de testes
